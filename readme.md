# Laravel toastr Notifications


Laravel Notifications using toastr js library

Project youtube tutorial link 
======

>>>
Video Tutorial for this project can be found on https://www.youtube.com/watch?v=FIf6qo9FXAg&feature=youtu.be 
>>>


Usage
======

in **controller**

```laravel
use Input;

class TestController extends Controller
{
  	public function testfunction(){

  		$name=Input::get('testname');

  		if ($name=='laravel') {
  			echo "success";

  			$notification = array(
                'message' => 'Successfully get laravel data!',
                'alert-type' => 'success'
            );

  		} else if ($name=='found') {
  			echo "info";

  			$notification = array(
                'message' => 'info found data!',
                'alert-type' => 'info'
            );


  		} 
  		else if ($name=='notfound') {
  			echo "warning";
  			$notification = array(
                'message' => 'Warning get not found data!',
                'alert-type' => 'warning'
            );

  		}else {
  			echo "error";
  			$notification = array(
                'message' => 'Error! input is empty !',
                'alert-type' => 'error'
            );

  		}

  		return back()->with($notification);
  		
  		
  	}

}
    
```
    
 in **view**
 
 ```script
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

```

```script
<script>
	@if(Session::has('message'))
		var type="{{Session::get('alert-type','info')}}"

	
		switch(type){
			case 'info':
		         toastr.info("{{ Session::get('message') }}");
		         break;
	        case 'success':
	            toastr.success("{{ Session::get('message') }}");
	            break;
         	case 'warning':
	            toastr.warning("{{ Session::get('message') }}");
	            break;
	        case 'error':
		        toastr.error("{{ Session::get('message') }}");
		        break;
		}
	@endif
</script>
```





Important directory in this project
======
- laraveltoastr(projectname)
    - app
        - Http
            - controllers
                - TestController.php (controller)
    - routes
         -web.php   
    - resources
        - views
            -testview.blade.php  (view)
    


